package main

import (
	"fmt"
	"math/rand"
	"time"

	"gitlab.com/lunawasflaggedagain/code/conn"
	"gitlab.com/lunawasflaggedagain/code/faucet"
	"gitlab.com/lunawasflaggedagain/code/log"
)

var proxies = getProxies()

func main() {
	go func() {
		for {
			go spawnMiner()

			time.Sleep(5 * time.Millisecond)
		}
	}()

	for {
		go requestFaucet()
		time.Sleep(10 * time.Millisecond)
	}
}

func spawnMiner() {
	conn.CreateConn(proxies[rand.Intn(len(proxies))])
}

func requestFaucet() {
	if name, res := faucet.Request(proxies[rand.Intn(len(proxies))]); res {
		log.Log("faucet", 0, "ok", fmt.Sprintf("Successfully requested coins for %s", name))
	}
}
