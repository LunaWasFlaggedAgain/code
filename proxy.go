package main

import (
	"io"
	"net/http"
	"strings"
)

func getProxies() []string {
	socks4 := getProxy("socks4")
	socks5 := getProxy("socks5")
	http := getProxy("http")

	for i, v := range socks4 {
		socks4[i] = "socks4://" + v
	}

	for i, v := range socks5 {
		socks5[i] = "socks5://" + v
	}

	for i, v := range http {
		http[i] = "http://" + v
	}

	socks := append(socks4, socks5...)
	return socks
	//return append(socks, http...)

}

func getProxy(t string) []string {
	resp, err := http.Get("https://api.proxyscrape.com/v2/?request=getproxies&protocol=" + t + "&timeout=10000&country=all")
	if err != nil {
		panic(err) // scream like a little baby
	}
	defer resp.Body.Close()

	body, err := io.ReadAll(resp.Body)

	if err != nil {
		panic(err)
	}

	return strings.Split(strings.ReplaceAll(string(body), "\r", ""), "\n")
}
