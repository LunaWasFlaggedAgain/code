package config

import (
	"io/ioutil"
	"strings"
)

// insert hashrate here. PLEASE KEEP THIS LOW else you'll run into issues with the automatic difficulty increaser.
const Hashrate = "10"

// insert "friendly" message in here
const Message = `jacked_by_luna`

// insert pools that you don't like in here
// typically you'd insert multiple so that they don't label you as a ddoser
// but you don't haaave to...
var Pools = []string{
	"149.91.88.18:1612",   // pulse-pool-1
	"149.91.88.18:6004",   // pulse-pool-2
	"50.112.145.154:6000", // beyond-pool-1
	"35.173.194.122:6000", // beyond-pool-2
}

var Usernames []string // mad?

func init() {
	/*resp, err := http.Get("https://server.duinocoin.com/api.json") //this pains me at the deepest levels of my heart -- Allink
	if err != nil {
		panic(err)
	}

	var res map[string]interface{}

	json.NewDecoder(resp.Body).Decode(&res)

	workers := res["Active workers"].(map[string]interface{})

	for key := range workers {
		Usernames = append(Usernames, key)
	}*/

	// lazy fix to make this work
	b, err := ioutil.ReadFile("./config/username.txt")
	if err != nil {
		panic(err)
	}

	split := strings.Split(strings.ReplaceAll(string(b), "\r", ""), "\n")

	Usernames = append(Usernames, split...)
}
