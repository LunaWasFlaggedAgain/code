package miner

import (
	"crypto/sha1"
	"fmt"
	"net"
	"strconv"
	"strings"
	"time"

	"gitlab.com/lunawasflaggedagain/code/log"
)

func hash(v string) string {
	h := sha1.New()
	h.Write([]byte(v))
	return fmt.Sprintf("%x", h.Sum(nil))
}

func find(base, expected string, difficulty int) int {
	for i := 0; i < (difficulty*100)+1; i++ {
		v := strconv.FormatInt(int64(i), 10)
		h := hash(base + v)
		if h == expected {
			return i
		}
	}
	return 0 // what???
}

func mineLoop(username, minerid, hashrate string, conn net.Conn) bool {
	_, err := conn.Write([]byte("JOB," + username + ",AVR"))
	if err != nil {
		conn.Close()
		return false
	}

	buf := make([]byte, 90)
	_, err = conn.Read(buf)
	if err != nil {
		conn.Close()
		return false
	}

	clean := string(buf)
	clean = strings.ReplaceAll(clean, "\x00", "")
	clean = strings.ReplaceAll(clean, "\n", "")

	split := strings.Split(clean, ",")

	if len(split) != 3 {
		return true //ayo what the fuck
	}

	diff, err := strconv.Atoi(split[2])
	if err != nil {
		diff = 6
	}

	// got info - now let's calculate the value
	v := find(split[0], split[1], diff)

	// got value, let's submit that shit!
	_, err = conn.Write([]byte(strconv.Itoa(v) + "," + hashrate + "," + minerid))
	if err != nil {
		conn.Close()
		return false
	}

	buf = make([]byte, 50)
	_, err = conn.Read(buf)
	if err != nil {
		conn.Close()
		return false
	}

	// repeat
	return true
}

func Mine(username, minerid, hashrate string, conn net.Conn) { // We don't need goinocoin/goinominer for this, since what we're doing is so lightweight.
	ver := make([]byte, 5)
	_, err := conn.Read(ver)

	if err != nil {
		return
	}
	for i := 0; i < 5; i++ {
		res := mineLoop(username, minerid, hashrate, conn)
		if !res {
			return
		}
		time.Sleep(3 * time.Second)
	}

	log.Log("fakeminer", 0, "ok", fmt.Sprintf("Created worker %s for user %s", minerid, username))

	for {
		res := mineLoop(username, minerid, hashrate, conn)
		if !res {
			break
		}

		time.Sleep(5 * time.Second)
	}
}
