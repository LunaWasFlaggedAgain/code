package conn

import (
	"fmt"
	"math/rand"
	"net/url"

	"gitlab.com/lunawasflaggedagain/code/config"
	"gitlab.com/lunawasflaggedagain/code/log"
	"gitlab.com/lunawasflaggedagain/code/miner"
	"golang.org/x/net/proxy"
)

func CreateDialer(proxyURI string) (proxy.Dialer, error) {
	uri, err := url.Parse(proxyURI)

	if err != nil {
		return nil, err
	}

	return proxy.FromURL(uri, proxy.Direct)
}

func CreateConn(proxyURI string) error {
	dialer, err := CreateDialer(proxyURI)

	if err != nil {
		return err
	}

	pool := config.Pools[rand.Intn(len(config.Pools))]

	conn, err := dialer.Dial("tcp", pool)
	if err != nil {
		return err
	}

	user := config.Usernames[rand.Intn(len(config.Usernames))]

	log.Log("fakeminer", 0, "debug", fmt.Sprintf("connected to pool %s using proxy %s", pool, proxyURI))

	miner.Mine(user, config.Message, config.Hashrate, conn)

	return nil

}
