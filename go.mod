module gitlab.com/lunawasflaggedagain/code

go 1.17

require (
	github.com/fatih/color v1.12.0
	golang.org/x/net v0.0.0-20210902165921-8d991716f632
)

require (
	github.com/mattn/go-colorable v0.1.8 // indirect
	github.com/mattn/go-isatty v0.0.12 // indirect
	golang.org/x/sys v0.0.0-20210423082822-04245dca01da // indirect
)
