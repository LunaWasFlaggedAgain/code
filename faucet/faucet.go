package faucet

import (
	"math/rand"

	"gitlab.com/lunawasflaggedagain/code/config"
	"gitlab.com/lunawasflaggedagain/code/conn"
	_ "gitlab.com/lunawasflaggedagain/code/faucet/faucets"
	"gitlab.com/lunawasflaggedagain/code/faucet/handler"
)

func Request(URL string) (string, bool) {
	dialer, err := conn.CreateDialer(URL)
	if err != nil {
		return "", false
	}

	usr := config.Usernames[rand.Intn(len(config.Usernames))]

	return usr, handler.Get(usr, dialer)
}
