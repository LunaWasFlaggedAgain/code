package faucets

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"

	"gitlab.com/lunawasflaggedagain/code/faucet/handler"
	"golang.org/x/net/proxy"
)

// This faucet is the only one with a sus check so this is a little bit bigger.

type Token struct {
	Token string `json:"token"`
}

func init() {
	handler.AddFaucet(handler.Faucet{
		Get: getAmogus,
	})
}

func amogus_getToken(client *http.Client) string {
	req, err := http.NewRequest("GET", "https://duco-faucet.pcgeek.pl/sus_check.php?sus_check_v3", nil)
	req.Header.Add("X-Requested-With", "XMLHttpRequest")
	if err != nil {
		return ""
	}

	resp, err := client.Do(req)
	if err != nil {
		return ""
	}

	var token Token
	json.NewDecoder(resp.Body).Decode(&token)
	return token.Token
}

func getAmogus(username string, dialer proxy.Dialer) bool {
	client := &http.Client{
		Transport: &http.Transport{
			Dial: dialer.Dial,
		},
	}

	req, err := http.NewRequest("GET", "https://duco-faucet.pcgeek.pl/faucet.php?nickname="+username, nil)
	if err != nil {
		return false
	}

	token := amogus_getToken(client)

	req.Header.Add("User-Agent", "Mozilla/5.0 (X11; Linux x86_64; rv:91.0) Gecko/20100101 Firefox/91.0")
	req.Header.Add("Referer", "https://duco-faucet.pcgeek.pl/")
	req.Header.Add("X-Requested-Policy", "not")
	req.Header.Add("X-Requested-With", "XMLHttpRequest")
	req.Header.Add("X-Super-User", "0")
	req.Header.Add("X-token", token)

	resp, err := client.Do(req)
	if err != nil {
		return false
	}

	defer resp.Body.Close()
	b, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		fmt.Println(err)
		return false
	}
	if strings.Contains(string(b), "txid") {
		return true
	}
	return false

}
