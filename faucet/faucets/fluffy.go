package faucets

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"

	"golang.org/x/net/proxy"
)

func init() {
	//handler.AddFaucet(handler.Faucet{
	//	Get: getFluffy,
	//})
}

func getFluffy(username string, dialer proxy.Dialer) bool {
	client := &http.Client{
		Transport: &http.Transport{
			Dial: dialer.Dial,
		},
	}

	req, err := http.NewRequest("GET", "https://ef90-24-133-160-161.ngrok.io/claim/"+username, nil)
	if err != nil {
		return false
	}

	resp, err := client.Do(req)
	if err != nil {
		return false
	}

	defer resp.Body.Close()
	b, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		fmt.Println(err)
		return false
	}
	if strings.Contains(string(b), "ducos to you enjoy") {
		return true
	}

	fmt.Println(string(b))
	return false
}
