package faucets

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"

	"gitlab.com/lunawasflaggedagain/code/faucet/handler"
	"golang.org/x/net/proxy"
)

func init() {
	handler.AddFaucet(handler.Faucet{
		Get: getFurim,
	})
}

func getFurim(username string, dialer proxy.Dialer) bool {
	client := &http.Client{
		Transport: &http.Transport{
			Dial: dialer.Dial,
		},
	}

	req, err := http.NewRequest("POST", "https://faucet.furim.xyz/giveMeDucos?ducoUsername="+username, nil)
	if err != nil {
		return false
	}

	resp, err := client.Do(req)
	if err != nil {
		return false
	}

	defer resp.Body.Close()
	b, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		fmt.Println(err)
		return false
	}
	if strings.Contains(string(b), "ducosSended") {
		return true
	}

	return false
}
