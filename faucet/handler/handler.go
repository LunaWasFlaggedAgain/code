package handler

import (
	"math/rand"

	"golang.org/x/net/proxy"
)

var faucets []Faucet

type Faucet struct {
	Get func(username string, dialer proxy.Dialer) bool
}

func AddFaucet(faucet Faucet) {
	faucets = append(faucets, faucet)

}

func Get(username string, dialer proxy.Dialer) bool {
	f := faucets[rand.Intn(len(faucets))].Get
	return f(username, dialer)
}
