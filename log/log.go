package log

import (
	"fmt"
	"os"
	"strings"

	clr "github.com/fatih/color"
)

func formatLog(name string, thread int, level, msg string) string {
	return fmt.Sprintf("[%s]\t[%d]\t[%s]\t\t| %s", name, thread, level, msg)
}

func Log(name string, thread int, level, msg string) {
	if level == "debug" && os.Getenv("DEBUG") == "" {
		return
	}

	var color func(format string, a ...interface{}) string

	switch level {
	case "debug":
		color = clr.BlackString
	case "info":
		color = clr.BlueString
	case "ok":
		color = clr.GreenString
	case "warn":
	case "alert":
		color = clr.YellowString
	case "error":
		color = clr.RedString
	}

	split := strings.Split(msg, "\n")

	for _, v := range split {
		fmt.Println(color(formatLog(name, thread, level, v)))
	}
}
